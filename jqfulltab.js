﻿/*! jqFulltab - v0.1.4 Beta - 2015/07/16 
* Jquery full page content tabs
* Copyright 2015 tungsteno74@yahoo.it Tungsteno; Licensed MIT */

(function ($, undefined) {
    $.widget("ui.tabs", $.ui.tabs, {
        _autoResizedTab: function () {
            this.tablist.each(function () {
                var contwidth = 0;
                $(this).find("> li:has(a[href])").each(function () {
                    contwidth += $(this).outerWidth(true);
                });
                $(this).width(contwidth+1);
            });
        },
        _create: function () {
            var ret = this._super();
            this._autoResizedTab();
            if (ret != undefined)
                return ret;
        }
    });
})(jQuery);